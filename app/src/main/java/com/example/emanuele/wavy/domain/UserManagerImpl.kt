package com.example.emanuele.wavy.domain

import com.example.emanuele.wavy.data.UserDataRepository
import com.example.emanuele.wavy.domain.model.User
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserManagerImpl @Inject constructor(private val dataRepository: UserDataRepository) : UserManager {

    override fun getUser(): Observable<User> =
            dataRepository.getUser()
                    .map { it -> User(it.id, it.firstName, it.lastName, it.profilePicture, it.phoneNumber, it.email) }

    override fun deleteUser(id: String): Observable<Boolean> =
            dataRepository.deleteUser(id)
                    .map { it.isSuccessful }
}