package com.example.emanuele.wavy.presentation

import android.databinding.Bindable
import android.databinding.Observable
import com.example.emanuele.wavy.R

interface IErrorView : Observable {

    @Bindable
    fun getErrorText(): Int?

    fun getErrorTextColor() = R.color.colorPrimaryDark

    fun getReloadText() = R.string.reload

    fun reload()

    @Bindable
    fun isErrorViewVisible(): Boolean
}