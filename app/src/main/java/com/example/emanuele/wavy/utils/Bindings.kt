package com.example.emanuele.wavy.utils

import android.databinding.BindingAdapter
import android.support.annotation.ColorRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.widget.TextView

@BindingAdapter("toTextColor")
fun TextView.setTextColorChecked(@ColorRes color: Int) {
    try {
        if (color != 0) {
            setTextColor(ContextCompat.getColor(context, color))
        }
    } catch (e: Exception) {
    }
}

@BindingAdapter("android:text")
fun TextView.setTextChecked(@StringRes stringRes: Int) {
    try {
        if (stringRes != 0) {
            setText(stringRes)
        }
    } catch (e: Exception) {
    }
}