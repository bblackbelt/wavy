package com.example.emanuele.wavy.presentation.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.DialogInterface
import android.databinding.Bindable
import com.blackbelt.bindings.notifications.MessageWrapper
import com.blackbelt.bindings.viewmodel.BaseViewModel
import com.example.emanuele.wavy.BR
import com.example.emanuele.wavy.R
import com.example.emanuele.wavy.domain.UserManager
import com.example.emanuele.wavy.domain.model.User
import com.example.emanuele.wavy.presentation.IErrorView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class MainViewModel constructor(private val userManager: UserManager) : BaseViewModel(), IErrorView {

    var imageUrl: String? = null
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.imageUrl)
        }


    var surname: String? = null
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.surname)
        }

    var firstname: String? = null
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.firstname)
        }

    var email: String? = null
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.email)
        }


    var phoneNumber: String? = null
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.phoneNumber)
        }

    var loading: Boolean = false
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.loading)
        }

    var deleteButtonVisible: Boolean = false
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.deleteButtonVisible)
        }

    var deleting: Boolean = false
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.deleting)
        }

    private var mUserDisposable = Disposables.disposed()

    private var mDeleteUserDisposable = Disposables.disposed()

    private var mUser: User? = null

    private var mErrorResId: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.errorText)
            notifyPropertyChanged(BR.errorViewVisible)
            notifyPropertyChanged(BR.userFound)
        }

    override fun onCreate() {
        super.onCreate()
        loadUser()
    }

    private fun loadUser() {
        loading = true
        mUserDisposable = userManager.getUser()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    loading = false
                    imageUrl = it.profilePicture
                    firstname = it.firstName
                    surname = it.lastName
                    phoneNumber = it.phoneNumber
                    email = it.email
                    mUser = it
                    deleteButtonVisible = true
                    notifyPropertyChanged(BR.userFound)
                }, {
                    handlerError(it)
                })
    }

    fun deleteUser() {
        mMessageNotifier.value = MessageWrapper.withDialog(
                R.string.delete_user,
                R.string.delete_user_message,
                0,
                android.R.string.ok,
                android.R.string.cancel,
                DialogInterface.OnClickListener { _, _ -> delete() }, null
        )
    }

    internal fun delete() {
        val user = mUser ?: return
        val id = user.id ?: return
        deleting = true
        mDeleteUserDisposable =
                userManager.deleteUser(id)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (it) {
                                deleting = false
                                email = null
                                phoneNumber = null
                                imageUrl = null
                                firstname = null
                                surname = null
                                mUser = null
                                deleteButtonVisible = false
                                notifyPropertyChanged(BR.userFound)
                                mMessageNotifier.value = MessageWrapper.withSnackBar(R.string.user_delete_succes, MessageWrapper.LENGTH_SHORT)
                            } else {
                                notifyMessageError(R.string.oops_something_went_wrong)
                            }
                        }, {
                            deleting = false
                            notifyMessageError(getErrorString(it))
                        })
    }

    private fun notifyMessageError(resId: Int) {
        mMessageNotifier.value = MessageWrapper.withSnackBar(resId, MessageWrapper.LENGTH_LONG)
    }

    internal fun getErrorString(throwable: Throwable) = if (throwable is HttpException || throwable is IOException) {
        R.string.connection_error
    } else {
        R.string.oops_something_went_wrong
    }

    @Bindable
    fun isUserFound(): Boolean {
        return !loading && mUser != null && !isErrorViewVisible()
    }

    override fun onDestroy() {
        super.onDestroy()
        mUserDisposable.dispose()
        mDeleteUserDisposable.dispose()
    }

    override fun handlerError(throwable: Throwable) {
        super.handlerError(throwable)
        loading = false
        mErrorResId = getErrorString(throwable)
    }

    @Bindable
    override fun getErrorText(): Int? = mErrorResId

    override fun reload() {
        mErrorResId = 0
        loadUser()
    }

    override fun getErrorTextColor(): Int = android.R.color.white

    @Bindable
    override fun isErrorViewVisible(): Boolean = mErrorResId > 0

    class Factory @Inject constructor(val userManager: UserManager) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = MainViewModel(userManager) as T
    }
}