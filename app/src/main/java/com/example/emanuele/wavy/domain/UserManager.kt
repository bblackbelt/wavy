package com.example.emanuele.wavy.domain

import com.example.emanuele.wavy.domain.model.User
import io.reactivex.Observable

interface UserManager {
    fun getUser(): Observable<User>
    fun deleteUser(id: String): Observable<Boolean>
}