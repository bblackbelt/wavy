package com.example.emanuele.wavy.di

import com.example.emanuele.wavy.presentation.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ContributorsModule {

    @ContributesAndroidInjector
    fun injectMainActivity(): MainActivity
}