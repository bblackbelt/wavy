package com.example.emanuele.wavy.utils

import android.databinding.BindingAdapter
import android.graphics.BitmapFactory
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.widget.ImageView
import com.example.emanuele.wavy.R
import com.squareup.picasso.Picasso

@BindingAdapter("srcUrl")
fun ImageView.loadImage(url: String?) {

    val placeHolder = BitmapFactory.decodeResource(resources, R.drawable.placeholder)
    val roundedPlaceholder = RoundedBitmapDrawableFactory.create(resources, placeHolder)
    roundedPlaceholder.isCircular = true

    if (url.isNullOrEmpty()) {
        setImageDrawable(roundedPlaceholder)
        return
    }

    Picasso.with(context)
            .load(url)
            .placeholder(roundedPlaceholder)
            .fit()
            .centerCrop()
            .transform(RoundedTransformation(url)).into(this)
}
