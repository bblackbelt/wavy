package com.example.emanuele.wavy.di

import com.example.emanuele.wavy.WavyApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class), (HelpersModule::class),
    (NetworkModule::class), (BindsModule::class), (ContributorsModule::class)])
interface AppComponent {

    fun inject(app: WavyApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: WavyApp): Builder

        fun build(): AppComponent
    }
}