package com.example.emanuele.wavy.domain.model

data class User(
        val id: String? = null,
        val firstName: String? = null,
        val lastName: String? = null,
        val profilePicture: String? = null,
        val phoneNumber: String? = null,
        val email: String? = null
)