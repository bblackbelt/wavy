package com.example.emanuele.wavy.data

import com.example.emanuele.wavy.data.model.User
import io.reactivex.Observable
import retrofit2.Response

interface UserDataRepository {

    fun getUser() : Observable<User>

    fun deleteUser(id : String) : Observable<Response<Void>>
}