package com.example.emanuele.wavy.presentation

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.blackbelt.bindings.activity.BaseBindingActivity
import com.example.emanuele.wavy.BR
import com.example.emanuele.wavy.R
import com.example.emanuele.wavy.presentation.viewmodel.MainViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : BaseBindingActivity() {

    @Inject
    lateinit var mFactory: MainViewModel.Factory

    private val mViewModel: MainViewModel  by lazy {
        ViewModelProviders.of(this, mFactory)[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main, BR.mainViewModel, mViewModel)
    }
}