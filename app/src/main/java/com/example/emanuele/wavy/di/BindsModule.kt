package com.example.emanuele.wavy.di

import com.example.emanuele.wavy.data.UserDataRepository
import com.example.emanuele.wavy.data.UserDataRepositoryImp
import com.example.emanuele.wavy.domain.UserManager
import com.example.emanuele.wavy.domain.UserManagerImpl
import dagger.Binds
import dagger.Module

@Module
abstract class BindsModule {

    @Binds
    abstract fun bindUserDataRepository(userDataRepository: UserDataRepositoryImp): UserDataRepository

    @Binds
    abstract fun bindUserManager(userManager: UserManagerImpl): UserManager

}