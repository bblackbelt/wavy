package com.example.emanuele.wavy.data

import com.example.emanuele.wavy.data.model.User
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DataService {

    @GET("/user/all")
    fun getUser(): Observable<User>

    @DELETE("user/{user_id}")
    fun deleteUser(@Path("user_id") id: String?) : Observable<Response<Void>>
}