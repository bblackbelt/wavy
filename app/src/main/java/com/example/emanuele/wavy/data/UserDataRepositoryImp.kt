package com.example.emanuele.wavy.data

import com.example.emanuele.wavy.data.model.User
import io.reactivex.Observable
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserDataRepositoryImp @Inject constructor(private val dataService: DataService) : UserDataRepository {

    override fun getUser(): Observable<User> = dataService.getUser()

    override fun deleteUser(id : String): Observable<Response<Void>> = dataService.deleteUser(id)
}