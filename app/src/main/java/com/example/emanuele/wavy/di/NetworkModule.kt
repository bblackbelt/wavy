package com.example.emanuele.wavy.di

import android.content.Context
import com.example.emanuele.wavy.data.DataService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import javax.inject.Singleton
import okhttp3.Cache

private const val HTTP_RESPONSE_DISK_CACHE_MAX_SIZE = (5 * 1024 * 1024).toLong()

@Module
class NetworkModule {

    @Provides
    fun provideCacheFile(context: Context): Cache {
        val baseDir = context.cacheDir
        val cacheDir = File(baseDir, "HttpResponseCache")
        return Cache(cacheDir, HTTP_RESPONSE_DISK_CACHE_MAX_SIZE)
    }

    @Provides
    @Singleton
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory = GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    fun providesRxJava2CallAdapter(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

    @Provides
    @Singleton
    fun provideUserDataService(context: Context, gson: GsonConverterFactory,
                               rxJava2CallAdapterFactory: RxJava2CallAdapterFactory): DataService {
        val builder = Retrofit.Builder()
                .baseUrl("http://private-anon-9adafcec7b-test16231.apiary-mock.com")
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gson)

        val retrofit = builder.build()
        return retrofit.create(DataService::class.java)
    }
}