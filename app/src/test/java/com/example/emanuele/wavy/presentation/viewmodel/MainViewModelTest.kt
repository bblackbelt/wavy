package com.example.emanuele.wavy.presentation.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.emanuele.wavy.R
import com.example.emanuele.wavy.domain.UserManager
import com.example.emanuele.wavy.domain.model.User
import io.reactivex.Observable
import org.junit.Assert
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    companion object {

        @ClassRule
        @JvmField
        val instantRule = InstantTaskExecutorRule()

        @ClassRule
        @JvmField
        val rxTestRule = RxTestRule()
    }

    @Mock
    lateinit var mUserManager: UserManager

    private val mMainViewModel: MainViewModel by lazy {
        MainViewModel(mUserManager)
    }

    @org.junit.Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun test_user_found() {

        val testId = "test_id"
        val firstName = "firstName"
        val lastName = "lastName"
        val profilePicture = "profilePicture"
        val phoneNumber = "phoneNumber"
        val email = "email"

        val user = User(testId, firstName, lastName, profilePicture, phoneNumber, email)

        Mockito.`when`(mUserManager.getUser())
                .thenReturn(Observable.just(user))

        mMainViewModel.onCreate()

        assertUser(user)
    }

    @Test
    fun test_get_user_fails() {

        Mockito.`when`(mUserManager.getUser())
                .thenReturn(Observable.error(IOException()))

        mMainViewModel.onCreate()

        assertUser()

        Assert.assertFalse(mMainViewModel.isUserFound())
        Assert.assertTrue(mMainViewModel.isErrorViewVisible())
    }

    @Test
    fun test_reload() {

        Mockito.`when`(mUserManager.getUser())
                .thenReturn(Observable.error(IOException()))

        mMainViewModel.onCreate()

        assertUser()

        Assert.assertFalse(mMainViewModel.isUserFound())
        Assert.assertTrue(mMainViewModel.isErrorViewVisible())

        val testId = "test_id"
        val firstName = "firstName"
        val lastName = "lastName"
        val profilePicture = "profilePicture"
        val phoneNumber = "phoneNumber"
        val email = "email"

        val user = User(testId, firstName, lastName, profilePicture, phoneNumber, email)

        Mockito.`when`(mUserManager.getUser())
                .thenReturn(Observable.just(user))

        mMainViewModel.reload()

        assertUser(user)

        Assert.assertTrue(mMainViewModel.isUserFound())
        Assert.assertFalse(mMainViewModel.isErrorViewVisible())
    }

    @Test
    fun test_on_error_generic() {

        val throwable = Throwable()

        Mockito.`when`(mUserManager.getUser())
                .thenReturn(Observable.error(throwable))

        mMainViewModel.onCreate()

        assertUser()

        Assert.assertEquals(mMainViewModel.getErrorString(throwable), R.string.oops_something_went_wrong)
    }

    @Test
    fun test_on_error_http() {

        val throwable = IOException()

        Mockito.`when`(mUserManager.getUser())
                .thenReturn(Observable.error(throwable))

        mMainViewModel.onCreate()

        assertUser()

        Assert.assertEquals(mMainViewModel.getErrorString(throwable), R.string.connection_error)
    }

    @Test
    fun test_delete_user_successful() {
        val testId = "test_id"
        val firstName = "firstName"
        val lastName = "lastName"
        val profilePicture = "profilePicture"
        val phoneNumber = "phoneNumber"
        val email = "email"

        val user = User(testId, firstName, lastName, profilePicture, phoneNumber, email)

        Mockito.`when`(mUserManager.getUser())
                .thenReturn(Observable.just(user))

        Mockito.`when`(mUserManager.deleteUser(testId))
                .thenReturn(Observable.just(true))

        mMainViewModel.onCreate()

        assertUser(user)

        mMainViewModel.delete()

        assertUser()

        Assert.assertFalse(mMainViewModel.deleteButtonVisible)
        Assert.assertFalse(mMainViewModel.isUserFound())
    }

    @Test
    fun test_delete_user_unsuccessful() {
        val testId = "test_id"
        val firstName = "firstName"
        val lastName = "lastName"
        val profilePicture = "profilePicture"
        val phoneNumber = "phoneNumber"
        val email = "email"

        val user = User(testId, firstName, lastName, profilePicture, phoneNumber, email)

        Mockito.`when`(mUserManager.getUser())
                .thenReturn(Observable.just(user))

        Mockito.`when`(mUserManager.deleteUser(testId))
                .thenReturn(Observable.just(false))

        mMainViewModel.onCreate()

        assertUser(user)

        mMainViewModel.delete()

        assertUser(user)

        Assert.assertTrue(mMainViewModel.deleteButtonVisible)
        Assert.assertTrue(mMainViewModel.isUserFound())
    }

    private fun assertUser(user: User? = null) {

        if (user != null) {

            Assert.assertNotNull(mMainViewModel.firstname)
            Assert.assertNotNull(mMainViewModel.surname)
            Assert.assertNotNull(mMainViewModel.imageUrl)
            Assert.assertNotNull(mMainViewModel.phoneNumber)
            Assert.assertNotNull(mMainViewModel.email)

            Assert.assertEquals(user.firstName, mMainViewModel.firstname)
            Assert.assertEquals(user.lastName, mMainViewModel.surname)
            Assert.assertEquals(user.profilePicture, mMainViewModel.imageUrl)
            Assert.assertEquals(user.phoneNumber, mMainViewModel.phoneNumber)
            Assert.assertEquals(user.email, mMainViewModel.email)

        } else {
            Assert.assertNull(mMainViewModel.firstname)
            Assert.assertNull(mMainViewModel.surname)
            Assert.assertNull(mMainViewModel.imageUrl)
            Assert.assertNull(mMainViewModel.phoneNumber)
            Assert.assertNull(mMainViewModel.email)
        }
    }
}