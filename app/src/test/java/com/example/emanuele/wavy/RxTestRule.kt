package com.example.emanuele.wavy.presentation.viewmodel

import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class RxTestRule : TestRule {

    private val mScheduler: Scheduler by lazy {
        Schedulers.trampoline()
    }

    override fun apply(base: Statement, description: Description): Statement {

        return object : Statement() {

            override fun evaluate() {
                RxJavaPlugins.setIoSchedulerHandler { scheduler -> mScheduler }
                RxJavaPlugins.setComputationSchedulerHandler { scheduler -> mScheduler }
                RxJavaPlugins.setNewThreadSchedulerHandler { scheduler -> mScheduler }
                RxJavaPlugins.setSingleSchedulerHandler { scheduler -> mScheduler }
                RxAndroidPlugins.setMainThreadSchedulerHandler { scheduler -> mScheduler }

                try {
                    base.evaluate()
                } finally {
                    RxJavaPlugins.reset()
                    RxAndroidPlugins.reset()
                }
            }
        }
    }
}